---
marp: true
---

# taro 使用 vue3 开发小程序

## 简介

**taro**是由京东提供的多端框架，与 uni-app 类似。但 uni-app 只支持 vue，而 taro 可以 vue 或 react。这个演示 dome，是我直接从官网下载，使用的是==taro-vue3-NutUI==模板。

## 使用依赖的相关文档

- [taro 官网](https://taro-docs.jd.com/taro/docs/README)
- [vue3 文档](https://vue3js.cn/docs/zh/guide/migration/introduction.html)
- [NutUI 组件官网](https://nutui.jd.com/#/)
- [tailwindcss 中文网](https://www.tailwindcss.cn/)

## vue3 主要语法变化
1.setup 函数算是 create、data、methods 区域的合并，像 watch,mounted 如果要使用，需要自己另外引入。
vue3
```ts
<template>
  ...
</template>

<script lang="ts">

import { ref, reactive, onMounted,toRefs } from "vue";
import { Button, Step, Steps, NoticeBar } from "vant";
export default {
  components: {
    Button,
    Step,
    Steps,
    NoticeBar,
  },
  setup() {
	//   响应式属性方法内设置默认值
    const nameinput = ref(0);
//     响应式对象
    const state = reactive({
	    ....
    });

	// 生命周期mounted
    onMounted(() => {
       ...
    });
//     定义方法
	const selectionStart=()=>{
		...
	}


    return {
	...toRefs(state)
      nameinput,
      selectionStart,

    };
  },
};
</script>

```

 

 
2.v-model用法改变 默认值从value变为modelValue,以前只能绑定一个值，现在除了默认的v-model，多其他值可以绑定。可以看pages/find/index下的组件封装案例。
```ts
<TestInput v-model="value" v-model:one="one" v-model:tow="tow" />
// 等价
<TestInput
:modelValue="value"
@update:modelValue="modelValue=$event"
:one="one"
@update:one="one=$event"
:tow="tow"
@update:tow="tow=$event"
/>
```
3.上下文函数可以代替部分vuex，vuex可以全局变量，但是在同一个页面跨组件传递数据用vuex有点浪费，所以vue3提供了上下文函数inject和provide，provide设置对象，inject获取对象。具体可以看pages/my/index的案例
```ts
// 设置对象
 const text = ref(0)
    const update = () => {
      text.value = text.value + 1
    }
  provide("father-text", readonly(text))
  provide("father-update", update)
//   获取对象并使用
 const text = inject("father-text")
   <input :value="text" />
````
4.不定状态组件和传送组件小程序尚未支持。

## tailwindcss

tailwindcss是一个原子化的css框架，最近有势头替代bootstrap.
像这样使用就可以设置高宽，如果不像使用，可以使用原生less、scss。文档会将这些常用的css的缩写和全写都罗列出来，也是学习css的一个入门手册。
```ts
<div class="  h-32 w-32  ">
  <!-- ... -->
</div>
```

