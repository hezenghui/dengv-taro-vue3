export default {
  pages: [
    'pages/index/index',
    'pages/home/index',
    'pages/my/index',
    'pages/find/index'
  ],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'black'
}
  
}
